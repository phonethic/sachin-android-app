package com.example.helpshiftexample;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.example.helpshiftexample.R;

import com.helpshift.Helpshift;

public class Main extends Activity {
	
	
	Button buttonFAQ;
	Button buttonInbox;
	Helpshift hs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		/*HashMap config = new HashMap();
	    config.put("enableInAppNotification", true);*/
		
		buttonFAQ = (Button) findViewById(R.id.buttonFAQ);
		buttonInbox = (Button) findViewById(R.id.buttonInbox);
		hs = new Helpshift(getApplicationContext());
		
		hs.install(Main.this, "66088533357fda76d828c7c3f70c044e", 
				"anirudh.helpshift.com", "anirudh_platform_20140120094347674-611236beb13980c");
		
		/*hs.install(getApplicationContext(), "66088533357fda76d828c7c3f70c044e", 
				"anirudh.helpshift.com", 
				"anirudh_platform_20140120094347674-611236beb13980c");*/
		
		buttonFAQ.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				/*Intent i = new Intent(Main.this, FaqPage.class);
				startActivity(i);*/
				hs.leaveBreadCrumb("Clicked on faq");
				hs.showFaqs(Main.this);
			}
		});
		
		buttonInbox.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				hs.leaveBreadCrumb("Clicked on inbox");
				//hs.showConversation(Main.this);
				//hs.showReportIssue(Main.this);
				hs.showInbox(Main.this);
			}
		});
				
		
		
		/*Helpshift.leaveBreadCrumb("started the app");
		Helpshift.setUserIdentifier("9967332622");*/
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	

}
