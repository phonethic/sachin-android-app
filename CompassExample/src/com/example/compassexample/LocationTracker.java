package com.example.compassexample;

import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.GeomagneticField;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.animation.RotateAnimation;
import android.widget.Toast;

public class LocationTracker extends Service implements LocationListener{

	Context context;
	
	final String TAG="LOCATION";
	
	boolean isGpsOn = false;
	
	boolean isNetworkOn = false;
	
	boolean canGetLoc = false;
	
	Location location;
	static Location targetLocation;
	GeomagneticField geoMagneticField;
	
	double latitude, longitude;
	
	final long MIN_DIST_FOR_UPDATE = 10;
	
	final long TIME_BETWEEN_UPDATE = 1000 * 60 * 1;
	
	LocationManager locationManager;
	
	
	
	public LocationTracker(Context context) {
		this.context = context;
		context.getPackageName();
		
		getLocation();
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onLocationChanged(Location location) {
		latitude = location.getLatitude();
		longitude = location.getLongitude();
	}

	@Override
	public void onProviderDisabled(String provider) {
		
	}
	
	@Override
	public void onProviderEnabled(String provider) {
		
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		
	}
	
	
	Location getLocation(){
		Log.i(TAG,"in getLocation()");
		try{
			locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
			
			
			/** Get GPS status */
			isGpsOn = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
			
			
			/** Get Network status */
			isNetworkOn = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
			
			if(isGpsOn==false && isNetworkOn==false){
				Log.i("No GPS / Network","Cannot get location");
			}
			else{
				if(isNetworkOn){
					Log.i(TAG,"in isNetyworkOn");
					locationManager.requestLocationUpdates(
							LocationManager.NETWORK_PROVIDER, 
							TIME_BETWEEN_UPDATE, MIN_DIST_FOR_UPDATE, 
							this);
					
					if(locationManager != null){
						location = locationManager.
										getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
						
						if(location != null){
							latitude = location.getLatitude();
							longitude = location.getLongitude();
							
							Log.i("LOCATION NETWORK",""+latitude+" "+longitude);
						}
					}
				}
				
				if(isGpsOn){
					Log.i(TAG,"in isGpsOn");
					if(location == null){
						locationManager.requestLocationUpdates(
								LocationManager.GPS_PROVIDER, 
								TIME_BETWEEN_UPDATE, 
								MIN_DIST_FOR_UPDATE, 
								this);
						
						if(locationManager != null){
							location = locationManager.
											getLastKnownLocation(LocationManager.GPS_PROVIDER);
							
							if(location != null){
								latitude = location.getLatitude();
								longitude = location.getLongitude();
								Log.i("LOCATION GPS",""+latitude+" "+longitude);
							}
						}
					}
				}
			}
		}
		catch(Exception e){e.printStackTrace();}
		
		return location;
	}
	
	double getLatitude(){
		
		return latitude;
	}
	
	double getLongitude(){
		return longitude;
	}
	
	
	void stopGPS(){
		if(locationManager != null){
			locationManager.removeUpdates(LocationTracker.this);
		}
	}
	
	

}
