package com.example.compassexample;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.os.Bundle;
import android.test.suitebuilder.annotation.SmallTest;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

@SuppressLint("NewApi")
public class Main extends Activity implements SensorEventListener, SurfaceHolder.Callback {

	private SensorManager mSensorManager;
	private Sensor mField, mAccelerometer;
	//private TextView valueView; 
	//TextView textViewAngleValue;

	private float[] mGravity;
	private float[] mMagnetic;

	LocationTracker locationTracker;

	double latitude, longitude;

	Location myLocation;
	Location targetLocation;
	ArrayList<Location> target;


	ImageView ivCompass;
	FrameLayout frameLayoutMarker;
	FrameLayout smallCompass;


	ArrayList<Float> angle;
	ArrayList<Float> distance;

	/* Array list that loads the n number of images for a view */
	ImageView[] markersNorth = new ImageView[5];
	ImageView[] markersNorthEast = new ImageView[5];
	ImageView[] markersEast = new ImageView[5];
	ImageView[] markersSouthEast = new ImageView[5];
	ImageView[] markersSouth = new ImageView[5];
	ImageView[] markersSouthWest = new ImageView[5];
	ImageView[] markersWest = new ImageView[5];
	ImageView[] markersNorthWest = new ImageView[5];

	float currentAngle = 0.0f;

	SurfaceView surfaceView;
	SurfaceHolder surfaceHolder;
	Camera camera;


	ArrayList<Float> lat;
	ArrayList<Float> longi;
	ArrayList<String> name;
	float azimuth;
	
	int i=0;
	
	boolean called_once = false;


	/* small circle image views */
	ImageView imageViewCircleSE1;
	ImageView imageViewCircleSE2;
	ImageView imageViewCircleSE3;
	ImageView imageViewCircleNW1;
	ImageView imageViewCircleNW2;
	ImageView imageViewCircleNW3;
	ImageView imageViewCircleNW4;
	ImageView imageViewCircleW1;
	ImageView imageViewCircleW2;
	ImageView imageViewCircleW3;
	ImageView imageViewCircleNE1;
	ImageView imageViewCircleNE2;
	ImageView imageViewCircleNE3;
	ImageView imageViewCircleN1;
	ImageView imageViewCircleN2;
	ImageView imageViewCircleN3;
	ImageView imageViewCircleE1;
	ImageView imageViewCircleE2;
	ImageView imageViewCircleE3;
	ImageView imageViewCircleSW1;
	ImageView imageViewCircleSW2;
	ImageView imageViewCircleSW3;
	ImageView imageViewCircleS1;
	ImageView imageViewCircleS2;
	ImageView imageViewCircleS3;
	
	ImageView ivMarker1; ImageView ivMarker2; ImageView ivMarker3;

	String direction = null;


	/* Array list for directions */
	ArrayList<Float> north, northEast, east, southEast,
	south, southWest, west, northWest;

	ArrayList<String> northName, northEastName, eastName, 
	southEastName, southName, southWestName, westName, northWestName;

	ArrayList<Float> northDist, northEastDist, eastDist, 
	southEastDist, southDist, southWestDist, westDist, northWestDist;


	/* Used for circle in small compass */
	private double x[];
	private double y[];

	int click;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		mSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mField = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);


		ivCompass = (ImageView) findViewById(R.id.imageViewSmallCompass);
		surfaceView = (SurfaceView) findViewById(R.id.surfaceView);
		frameLayoutMarker = (FrameLayout) findViewById(R.id.markerFrame);
		smallCompass = (FrameLayout) findViewById(R.id.smallCompass);

		/** Initialize circles of compass */
		imageViewCircleSE1 = (ImageView) findViewById(R.id.imageViewCircleSE1);
		imageViewCircleSE2 = (ImageView) findViewById(R.id.imageViewCircleSE2);
		imageViewCircleSE3 = (ImageView) findViewById(R.id.imageViewCircleSE3);
		imageViewCircleNW1 = (ImageView) findViewById(R.id.imageViewCircleNW1);
		imageViewCircleNW2 = (ImageView) findViewById(R.id.imageViewCircleNW2);
		imageViewCircleNW3 = (ImageView) findViewById(R.id.imageViewCircleNW3);
		imageViewCircleNW4 = (ImageView) findViewById(R.id.imageViewCircleNW4);
		imageViewCircleW1 = (ImageView) findViewById(R.id.imageViewCircleW1);
		imageViewCircleW2 = (ImageView) findViewById(R.id.imageViewCircleW2);
		imageViewCircleW3 = (ImageView) findViewById(R.id.imageViewCircleW3);
		imageViewCircleNE1 = (ImageView) findViewById(R.id.imageViewCircleNE1);
		imageViewCircleNE2 = (ImageView) findViewById(R.id.imageViewCircleNE2);
		imageViewCircleNE3 = (ImageView) findViewById(R.id.imageViewCircleNE3);
		imageViewCircleN1 = (ImageView) findViewById(R.id.imageViewCircleN1);
		imageViewCircleN2 = (ImageView) findViewById(R.id.imageViewCircleN2);
		imageViewCircleN3 = (ImageView) findViewById(R.id.imageViewCircleN3);
		imageViewCircleE1 = (ImageView) findViewById(R.id.imageViewCircleE1);
		imageViewCircleE2 = (ImageView) findViewById(R.id.imageViewCircleE2);
		imageViewCircleE3 = (ImageView) findViewById(R.id.imageViewCircleE3);
		imageViewCircleSW1 = (ImageView) findViewById(R.id.imageViewCircleSW1);
		imageViewCircleSW2 = (ImageView) findViewById(R.id.imageViewCircleSW2);
		imageViewCircleSW3 = (ImageView) findViewById(R.id.imageViewCircleSW3);
		imageViewCircleS1 = (ImageView) findViewById(R.id.imageViewCircleS1);
		imageViewCircleS2 = (ImageView) findViewById(R.id.imageViewCircleS2);
		imageViewCircleS3 = (ImageView) findViewById(R.id.imageViewCircleS3);
		ivMarker1 = (ImageView) findViewById(R.id.imageViewMarker1);
		ivMarker2 = (ImageView) findViewById(R.id.imageViewMarker2);
		ivMarker3 = (ImageView) findViewById(R.id.imageViewMarker3);
		
		/** Initialize degrees, name and distance for array lists */
		lat = new ArrayList<Float>();
		longi = new ArrayList<Float>();
		target = new ArrayList<Location>();
		name = new ArrayList<String>();


		north = new ArrayList<Float>();
		northEast = new ArrayList<Float>();
		east = new ArrayList<Float>();
		southEast = new ArrayList<Float>();
		south = new ArrayList<Float>();
		southWest = new ArrayList<Float>();
		west = new ArrayList<Float>();
		northWest = new ArrayList<Float>();


		northName = new ArrayList<String>();
		northEastName = new ArrayList<String>();
		eastName = new ArrayList<String>();
		southEastName = new ArrayList<String>();
		southName = new ArrayList<String>();
		southWestName = new ArrayList<String>();
		westName = new ArrayList<String>();
		northWestName = new ArrayList<String>();


		northDist = new ArrayList<Float>();
		northEastDist = new ArrayList<Float>();
		eastDist = new ArrayList<Float>();
		southEastDist = new ArrayList<Float>();
		southDist = new ArrayList<Float>();
		southWestDist = new ArrayList<Float>();
		westDist = new ArrayList<Float>();
		northWestDist = new ArrayList<Float>();


		distance = new ArrayList<Float>();
		angle = new ArrayList<Float>();


		/** Get GPS location */
		locationTracker = new LocationTracker(getApplicationContext());
		locationTracker.getLocation();
		latitude = locationTracker.getLatitude(); 
		longitude = locationTracker.getLongitude();


		/** Set target location */
		myLocation = new Location("");
		targetLocation = new Location("");

		myLocation.setLatitude(latitude);
		myLocation.setLongitude(longitude);


		/*targetLocation.setLatitude(19.1439256);
		targetLocation.setLongitude(72.8245479);*/

		/** adults art, ferns n petals, esbeda */
		lat.add((float) 19.1439256);
		longi.add((float) 72.8245479);
		name.add("Ferns N Petals");

		/** pleasure treasure */
		/*lat.add((float) 19.1442188);
		longi.add((float) 72.8247884);
		name.add("Pleasure Treasure");*/

		/** The wedding and Cafe Lounge */
		lat.add((float) 19.134434);
		longi.add((float) 72.831962);
		name.add("The wedding and cafe lounge");

		/** theobroma */
		lat.add((float) 19.1439256);
		longi.add((float) 72.8245479);
		name.add("Theobroma");

		/** star bazaar */
		lat.add((float) 19.130887);
		longi.add((float) 72.831071);
		name.add("Star Bazaar");

		/** garden court */
		lat.add((float) 19.128302);
		longi.add((float) 72.837784);
		name.add("Garden Court");

		/** jugheads */
		/*lat.add((float) 19.131756);
		longi.add((float) 72.835613);
		name.add("Jugheads");*/

		/** Apna Bazaar */
		lat.add((float) 19.127988);
		longi.add((float) 72.832434);
		name.add("Apna Bazaar");

		findLocationAngles();

		showCompassCircle();

		/** Set up camera */
		surfaceHolder=surfaceView.getHolder();
		surfaceHolder.addCallback(this);
		surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
		surfaceHolder.setKeepScreenOn(true);
	}


	@Override
	protected void onStart() {
		super.onStart();
		
		ivMarker2.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String tn = northWestName.get(1);
				String d = northWestDist.get(1).toString();
				showDialog(tn, d);
			}
		});
		
		ivMarker1.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				Log.i("Name","Shop name: "+northWestName.get(0));
				String tn = northWestName.get(0);
				String d = northWestDist.get(0).toString();
				//showDialog("Ferns and Petals", d);
				
				/*
				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
						Main.this);

				*//** set title as shop name *//*
				alertDialogBuilder.setTitle("Ferns and Petals");

				*//** set message as distance *//*
				alertDialogBuilder
				.setMessage("Distance: "+distance)
				.setCancelable(true)
				.setNegativeButton("OK",new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog,int id) {
						dialog.cancel();
					}
				});

				AlertDialog alertDialog = alertDialogBuilder.create();
				alertDialog.show();*/
				
			}
		});
		
		
		/*
		ivMarker3.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String tn = northWestName.get(2);
				String d = northWestDist.get(2).toString();
				showDialog(tn, d);
			}
		});*/
	}
	


	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
	}


	
	protected void onResume() {
		super.onResume();
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
		mSensorManager.registerListener(this, mField, SensorManager.SENSOR_DELAY_UI);
	}


	
	protected void onPause() {
		super.onPause();
		mSensorManager.unregisterListener(this);
	}


	
	private void updateDirection2() {
		float[] temp = new float[9];
		float[] R = new float[9];

		//Load rotation matrix into R
		SensorManager.getRotationMatrix(temp, null, mGravity, mMagnetic);
		//Remap to camera's point-of-view
		SensorManager.remapCoordinateSystem(temp, SensorManager.AXIS_X, SensorManager.AXIS_Z, R);
		//Return the orientation values
		float[] values = new float[3];
		SensorManager.getOrientation(R, values);
		//Convert to degrees
		for (int i=0; i < values.length; i++) {
			Double degrees = (values[i] * 180) / Math.PI;
			values[i] = degrees.floatValue();
		}

		//float azimuth = values[0];

		//Log.i("Direction",""+
		getDirectionFromDegrees(values[0]);
	}



	void rotateCompass(float angle){

		RotateAnimation rotate = new RotateAnimation(
				currentAngle, 
				angle,
				Animation.RELATIVE_TO_SELF,
				0.5f,
				Animation.RELATIVE_TO_SELF,
				0.5f);

		rotate.setDuration(1000);
		smallCompass.startAnimation(rotate);
		currentAngle = angle;

	}


	
	private String getDirectionFromDegrees(float degrees) {

		if(degrees >= -22.5 && degrees < 22.5) { 
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();


			/** Rotate compass by the angle */
			rotateCompass(0);

			/** get shop name and distance */
			for(int i=0; i<northDist.size();i++){
				Log.i("Shop Info", "Name: "+northName.get(i));
				Log.i("Shop Info", "Dist: "+northDist.get(i));
			}

			/** ArrayList of markers */
			for(int i=0; i<northDist.size(); i++){
				ImageView iv = new ImageView(getApplicationContext());
				iv.setImageResource(R.drawable.mapmarker);

				iv.setPadding(i, i, i, i);

				frameLayoutMarker.addView(iv);
			}

			for(int i=0; i<northDist.size(); i++){
				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);


				markersNorth[i] = iv; 
				markersNorth[i].setId(i);

				if(i % 2 ==0)
					markersNorth[i].setPadding(i*50, 0, 0, 0);
				else
					markersNorth[i].setPadding(i*50, 50, 0, 0);

				//markers[i].setPadding(left, top, right, bottom);

				markersNorth[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.i("Click", "image click "+click);
						String tn = northName.get(v.getId());
						String d = northDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});

				frameLayoutMarker.addView(markersNorth[i]);

				if(i==0){
					frameLayoutMarker.setVisibility(View.VISIBLE);
				}

			}

			return "N";
		}
		else{
			ivMarker1.setVisibility(View.GONE);
			ivMarker2.setVisibility(View.GONE);
			ivMarker3.setVisibility(View.GONE);
		}
		
		if(degrees >= 22.5 && degrees < 67.5) { 
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();


			rotateCompass(45);

			for(int i=0; i<northEastDist.size();i++){
				Log.i("Shop Info", "Name: "+northEastName.get(i));
				Log.i("Shop Info", "Dist: "+northEastDist.get(i));
			}

			for(int i=0; i<northEastDist.size(); i++){
				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);


				markersNorthEast[i] = iv;
				markersNorthEast[i].setId(i);

				if(i % 2 ==0)
					markersNorthEast[i].setPadding(i*50, 0, 0, 0);
				else
					markersNorthEast[i].setPadding(i*50, 50, 0, 0);

				//markers[i].setPadding(left, top, right, bottom);

				markersNorthEast[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.i("Click", "image click "+click);
						String tn = northEastName.get(v.getId());
						String d = northEastDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});

				frameLayoutMarker.addView(markersNorthEast[i]);

				if(i==0){
					frameLayoutMarker.setVisibility(View.VISIBLE);
				}

			}

			return "NE";
		}
		else{
			ivMarker1.setVisibility(View.GONE);
			ivMarker2.setVisibility(View.GONE);
			ivMarker3.setVisibility(View.GONE);
		}
		
		if(degrees >= 67.5 && degrees < 112.5) { 
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();

			rotateCompass(90);

			for(int i=0; i<east.size(); i++){
				Log.i("Shop Info", "Name: "+eastName.get(i));
				Log.i("Shop Info", "Dist: "+eastDist.get(i));
			}

			for(int i=0; i<eastDist.size(); i++){

				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);


				markersEast[i] = iv;
				markersEast[i].setId(i);

				if(i % 2 ==0)
					markersEast[i].setPadding(i*50, 0, 0, 0);
				else
					markersEast[i].setPadding(i*50, 50, 0, 0);

				//markers[i].setPadding(left, top, right, bottom);

				markersEast[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.i("Click", "image click "+click);
						String tn = eastName.get(v.getId());
						String d = eastDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});

				frameLayoutMarker.addView(markersEast[i]);

				if(i==0){
					frameLayoutMarker.setVisibility(View.VISIBLE);
				}

			}

			//Log.i("Direction","e");
			return "E";
		}
		else{
			ivMarker1.setVisibility(View.GONE);
			ivMarker2.setVisibility(View.GONE);
			ivMarker3.setVisibility(View.GONE);
		}
		
		if(degrees >= 112.5 && degrees < 157.5) { // garden court
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();

			rotateCompass(135);

			for(int i=0; i<southEastDist.size();i++){
				Log.i("Shop Info", "Name: "+southEastName.get(i));
				Log.i("Shop Info", "Dist: "+southEastDist.get(i));
			}

			for(int i=0; i<southEastDist.size(); i++){
				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);


				markersSouthEast[i] = iv;
				markersSouthEast[i].setId(i);

				if(i % 2 ==0)
					markersSouthEast[i].setPadding(i*50, 0, 0, 0);
				else
					markersSouthEast[i].setPadding(i*50, 50, 0, 0);

				//markers[i].setPadding(left, top, right, bottom);

				markersSouthEast[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.i("Click", "image click "+click);
						String tn = southEastName.get(v.getId());
						String d = southEastDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});

				frameLayoutMarker.addView(markersSouthEast[i]);

				if(i==0){
					frameLayoutMarker.setVisibility(View.VISIBLE);
				}

			}

			return "SE";
		}
		else{
			ivMarker1.setVisibility(View.GONE);
			ivMarker2.setVisibility(View.GONE);
			ivMarker3.setVisibility(View.GONE);
		}
		
		if(degrees >= 157.5 || degrees < -157.5) { 
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();


			rotateCompass(180);

			for(int i=0; i<southDist.size();i++){
				Log.i("Shop Info", "Name: "+southName.get(i));
				Log.i("Shop Info", "Dist: "+southDist.get(i));
			}

			for(int i=0; i<southDist.size(); i++){

				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);


				markersSouth[i] = iv;
				markersSouth[i].setId(i);

				if(i % 2 ==0)
					markersSouth[i].setPadding(i*50, 0, 0, 0);
				else
					markersSouth[i].setPadding(i*50, 50, 0, 0);

				//markers[i].setPadding(left, top, right, bottom);

				markersSouth[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.i("Click", "image click "+click);
						String tn = southName.get(v.getId());
						String d = southDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});

				frameLayoutMarker.addView(markersSouth[i]);

				if(i==0){
					frameLayoutMarker.setVisibility(View.VISIBLE);
				}

			}

			//Log.i("Direction","s");
			return "S";
		}
		else{
			ivMarker1.setVisibility(View.GONE);
			ivMarker2.setVisibility(View.GONE);
			ivMarker3.setVisibility(View.GONE);
		}
		
		if(degrees >= -157.5 && degrees < -112.5) { 
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();


			rotateCompass(-135);

			for(int i=0; i<southWestDist.size();i++){
				Log.i("Shop Info", "Name: "+southWestName.get(i));
				Log.i("Shop Info", "Dist: "+southWestDist.get(i));
			}

			for(int i=0; i<southWestDist.size(); i++){
				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);


				markersSouthWest[i] = iv;
				markersSouthWest[i].setId(i);

				if(i % 2 ==0)
					markersSouthWest[i].setPadding(i*50, 0, 0, 0);
				else
					markersSouthWest[i].setPadding(i*50, 50, 0, 0);

				//markers[i].setPadding(left, top, right, bottom);

				markersSouthWest[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.i("Click", "image click "+click);
						String tn = southWestName.get(v.getId());
						String d = southWestDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});

				frameLayoutMarker.addView(markersSouthWest[i]);

				if(i==0){
					frameLayoutMarker.setVisibility(View.VISIBLE);
				}

			}

			//Log.i("Direction","sw");
			return "SW";
		}
		else{
			ivMarker1.setVisibility(View.GONE);
			ivMarker2.setVisibility(View.GONE);
			ivMarker3.setVisibility(View.GONE);
		}
		
		if(degrees >= -112.5 && degrees < -67.5) { //star bazaar
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();


			rotateCompass(270);
			//Toast.makeText(this, "Star Bazaar", Toast.LENGTH_LONG).show();

			for(int i=0; i<westDist.size();i++){
				Log.i("Shop Info", "Name: "+westName.get(i));
				Log.i("Shop Info", "Dist: "+westDist.get(i));
			}
			Log.i("size","star bazaar size "+westDist.size());

			for(int i=0; i<westDist.size(); i++){
				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);
				

				markersWest[i] = iv;
				markersWest[i].setId(i);

				if(i % 2 ==0)
					markersWest[i].setPadding(i*50, 0, 0, 0);
				else
					markersWest[i].setPadding(i*50, 50, 0, 0);

				//markers[i].setPadding(left, top, right, bottom);

				frameLayoutMarker.addView(markersWest[i]);

				if(i==0){
					frameLayoutMarker.setVisibility(View.VISIBLE);
				}

				markersWest[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						Log.i("Click", "image click "+click);
						String tn = westName.get(v.getId());
						String d = westDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});

			}

			//Log.i("Direction","w");
			return "W";
		}
		else{
			ivMarker1.setVisibility(View.GONE);
			ivMarker2.setVisibility(View.GONE);
			ivMarker3.setVisibility(View.GONE);
		}
		
		if(degrees >= -67.5 && degrees < -22.5) { // 3 shops of sl
			frameLayoutMarker.setVisibility(View.GONE);
			frameLayoutMarker.removeAllViewsInLayout();

			direction = "nw";

			rotateCompass(315);

			ivMarker1.setVisibility(View.VISIBLE);
			ivMarker2.setVisibility(View.VISIBLE);
			ivMarker3.setVisibility(View.VISIBLE);
			
			/*
			for(int i=0; i<northWestDist.size(); i++){
				ImageView iv = new ImageView(this);
				iv.setImageResource(R.drawable.mapmarkersmall);

				markersNorthWest[i] = iv;
				//Set id for image markers
				markersNorthWest[i].setId(i);

				if(i % 2 == 0){
					markersNorthWest[i].setPadding(i*100, 0, 0, 0);
				}
				else 
					markersNorthWest[i].setPadding(i*100, 100, 0, 0);
				
				frameLayoutMarker.setVisibility(View.VISIBLE);
				Log.i("TEMP","Image view size "+markersNorthWest[i].getId());

				frameLayoutMarker.addView(markersNorthWest[i]);
			}
			
			for(i=0; i<northWestDist.size() ; i++){
				markersNorthWest[i].setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						
						Log.i("Click", "image click "+v.getId());
						String tn = northWestName.get(v.getId());
						String d = northWestDist.get(v.getId()).toString();
						showDialog(tn, d);
					}
				});
			}
			*/
			return "NW";
		}
		

		return null;
	}


	public void onAccuracyChanged(Sensor sensor, int accuracy) { }


	public void onSensorChanged(SensorEvent event) {
		switch(event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			mGravity = event.values.clone();
			break;
		case Sensor.TYPE_MAGNETIC_FIELD:
			mMagnetic = event.values.clone();
			break;
		default:
			return;
		}
		if(mGravity != null && mMagnetic != null) {
			if(event.values[2]>-5.0 && event.values[2]<5.0 ){
					updateDirection2();
			}
			/*else
				frameLayoutMarker.removeAllViewsInLayout();*/
		}

	}

	
	/* Find degree for target locations */
	void findLocationAngles(){ 

		/** Find angle and distance */
		for(int i=0; i < lat.size(); i++ ){
			targetLocation.setLatitude(lat.get(i));
			targetLocation.setLongitude(longi.get(i));

			distance.add(myLocation.distanceTo(targetLocation));

			angle.add(myLocation.bearingTo(targetLocation));

			Log.i("Shops"," Angle: "+angle.get(i)+" Name: "+name.get(i));
		}

		/** Assign angles to direction */
		for(int i=0 ; i<angle.size(); i++){

			float angleTemp = angle.get(i);
			float distTemp = distance.get(i);
			String nameTemp = name.get(i);

			if(angleTemp >= -22.5 && angleTemp < 22.5) { //n
				northDist.add(distTemp);
				northName.add(nameTemp);
			}

			if(angleTemp >= 22.5 && angleTemp < 67.5) { //ne
				northEastDist.add(distTemp);
				northEastName.add(nameTemp);
			}

			if(angleTemp >= 67.5 && angleTemp < 112.5) { //e
				eastDist.add(distTemp);
				eastName.add(nameTemp);
			}

			if(angleTemp >= 112.5 && angleTemp < 157.5) { //se
				southEastDist.add(distTemp);
				southEastName.add(nameTemp);
			}

			if(angleTemp >= 157.5 || angleTemp < -157.5) { //s
				southDist.add(distTemp);
				southName.add(nameTemp);
			}

			if(angleTemp >= -157.5 && angleTemp < -112.5) { //sw
				southWestDist.add(distTemp);
				southWestName.add(nameTemp);
			}

			if(angleTemp >= -112.5 && angleTemp < -67.5) { //w
				westDist.add(distTemp);
				westName.add(nameTemp);
			}

			if(angleTemp >= -67.5 && angleTemp < -22.5) { //nw
				northWestDist.add(distTemp);
				northWestName.add(nameTemp);
				Log.i("NorthWest","Dist: "+northWestDist.get(0));
				Log.i("NorthWest","Name: "+northWestName.get(0));
				
			}
		}

	}


	
	void showCompassCircle(){

		if(northDist.size()>0){
			ImageView[] smallCircle = {imageViewCircleN1, imageViewCircleN2, imageViewCircleN3};

			for(int i=0; i<northDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}

		if(northEastDist.size()>0){
			ImageView[] smallCircle = {imageViewCircleNE1, imageViewCircleNE2, imageViewCircleNE3};

			for(int i=0; i<northEastDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}

		if(eastDist.size()>0){
			ImageView[] smallCircle = {imageViewCircleE1, imageViewCircleE2, imageViewCircleE3};

			for(int i=0; i<eastDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}

		if(southEastDist.size()>0){
			ImageView[] smallCircle = {imageViewCircleSE1, imageViewCircleSE2, imageViewCircleSE3};

			for(int i=0; i<southEastDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}

		if(south.size()>0){
			ImageView[] smallCircle = {imageViewCircleS1, imageViewCircleS2, imageViewCircleS3};

			for(int i=0; i<southDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}

		if(southWestDist.size()>0){
			ImageView[] smallCircle = {imageViewCircleSW1, imageViewCircleSW2, imageViewCircleSW3};

			for(int i=0; i<southWestDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}

		if(westDist.size()>0){
			ImageView[] smallCircle = {imageViewCircleW1, imageViewCircleW2, imageViewCircleW1};

			for(int i=0; i<westDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}

		if(northWestDist.size()>0){
			ImageView[] smallCircle = {imageViewCircleNW1, imageViewCircleNW2, imageViewCircleNW3,
					imageViewCircleNW4};

			for(int i=0; i<northWestDist.size(); i++){
				smallCircle[i].setVisibility(View.VISIBLE);
			}
		}
	}


	
	//===================================================================
	/* Camera */


	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		startCamera();
	}



	void showDialog(String name, String distance){

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				Main.this);

		/** set title as shop name */
		alertDialogBuilder.setTitle(name);

		/** set message as distance */
		alertDialogBuilder
		.setMessage("Distance: "+distance)
		.setCancelable(true)
		.setNegativeButton("OK",new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog,int id) {
				dialog.cancel();
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();

	}


	
	@Override
	public void surfaceCreated(SurfaceHolder arg0) {

	}


	
	@Override
	public void surfaceDestroyed(SurfaceHolder arg0) {
		stopCamera();
	}

	/** Camera functions */
	

	void startCamera(){

		try{
			camera=Camera.open();
		}
		catch(Exception e){}


		camera.setDisplayOrientation(90);

		try{
			camera.setPreviewDisplay(surfaceHolder);
		}catch(Exception e){}
		camera.startPreview();
	}



	void stopCamera(){
		camera.stopPreview();
		camera.release();
	}
}