package com.example.googleplusexample;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.plus.PlusClient;

public class Main extends Activity implements PlusClient.ConnectionCallbacks, 
PlusClient.OnConnectionFailedListener,
PlusClient.OnAccessRevokedListener {

	SignInButton buttonGooglePlus;
	ConnectionResult connectionResult;

	PlusClient plusClient;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);


		plusClient = new PlusClient.Builder(this,this, this)
		.setActions(MomentUtil.ACTIONS)
		.build();

		buttonGooglePlus = (SignInButton) findViewById(R.id.buttonGooglePlus);

		buttonGooglePlus.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				int available = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
				if (available != ConnectionResult.SUCCESS) {
					//showDialog(DIALOG_GET_GOOGLE_PLAY_SERVICES);
					return;
				}

				try {
					// mSignInStatus.setText(getString(R.string.signing_in_status));
					connectionResult.startResolutionForResult(Main.this, 1);
				} catch (IntentSender.SendIntentException e) {
					// Fetch a new result to start.
					plusClient.connect();
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onStart() {
		super.onStart();
		plusClient.connect();
	}

	@Override
	public void onStop() {
		plusClient.disconnect();
		super.onStop();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		//if (requestCode == 1
				//|| requestCode == REQUEST_CODE_GET_GOOGLE_PLAY_SERVICES) {
			//if (resultCode == RESULT_OK && !mPlusClient.isConnected()
					//&& !mPlusClient.isConnecting()) {
				// This time, connect should succeed.
				plusClient.connect();
			//}
		//}
	}



	@Override
	public void onAccessRevoked(ConnectionResult status) {
		plusClient.connect();
	}


	@Override
	public void onConnectionFailed(ConnectionResult result) {
		connectionResult = result;
		Log.e("Result failed", ""+result);
	}


	@Override
	public void onConnected(Bundle connectionHint) {
		Log.i("User name",""+plusClient.getCurrentPerson().getDisplayName());
	}


	@Override
	public void onDisconnected() {

	}

}
