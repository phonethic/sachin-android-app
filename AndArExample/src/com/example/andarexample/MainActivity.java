package com.example.andarexample;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import edu.dhbw.andar.AndARActivity;

public class MainActivity extends AndARActivity {

    @Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CustomRenderer renderer = new CustomRenderer();
        super.setNonARRenderer(renderer);
        
		startPreview();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


	@Override
	public void uncaughtException(Thread arg0, Throwable ex) {
		Log.e("AndAR EXCEPTION", ex.getMessage());
		finish();
		
	}
    
}
